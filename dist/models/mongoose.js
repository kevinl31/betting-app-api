"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
// if (process.env.DEBUG === 'true') {
// mongoose.set('debug', true);
// }
if (process.env.NODE_ENV === 'test') {
    mongoose.connect(process.env.MONGODB_URI_TEST, {
        useNewUrlParser: true
    }).then(() => console.log("connection succed"), (err) => console.log("connection failed: ", err));
}
else {
    mongoose.connect(process.env.MONGODB_URI, { useNewUrlParser: true }).then(() => console.log("connection succed"), (err) => console.log("connection failed: ", err));
    ;
}
exports.default = mongoose;
