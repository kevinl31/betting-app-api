"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const TeamStandingsStatsSchema = new mongoose_1.Schema({
    position: Number,
    team: {
        id: Number,
        name: String,
        crestUrl: String
    },
    playedGames: Number,
    won: Number,
    draw: Number,
    lost: Number,
    points: Number,
    goalsFor: Number,
    goalsAgainst: Number,
    goalDifference: Number
});
const StandingsSchema = new mongoose_1.Schema({
    stage: String,
    type: String,
    table: [TeamStandingsStatsSchema]
});
const GlobalStandingsSchema = new mongoose_1.Schema({
    competition: {
        competitionId: Number,
        name: String
    },
    season: {
        seasonId: Number,
        startDate: Date,
        endDate: Date,
        currentMatchday: Number
    },
    standings: [StandingsSchema],
    lastUpdate: Date
});
const GlobalStandings = mongoose_1.model('GlobalStandings', GlobalStandingsSchema);
exports.default = GlobalStandings;
