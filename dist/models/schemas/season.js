"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const SeasonSchema = new mongoose_1.Schema({
    footballApiId: {
        type: Number,
        unique: true
    },
    startDate: Date,
    endDate: Date,
    currentMatchDay: Number,
    winner: {
        teamId: Number,
        name: String,
        shortName: String,
        tla: String,
        crestUrl: String
    },
    competition: {
        competitionId: Number,
        name: String
    },
    lastUpdate: Date
});
const Season = mongoose_1.model('Season', SeasonSchema);
exports.default = Season;
