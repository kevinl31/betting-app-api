"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const CompetitionSchema = new mongoose_1.Schema({
    footballApiId: {
        type: Number,
        unique: true
    },
    name: String,
    emblemUrl: String,
    currentSeason: {
        seasonId: Number,
        startDate: Date,
        endDate: Date,
        currentMatchday: Number
    },
    numberOfAvailableSeason: Number,
    lasUpdate: Date
});
const Competition = mongoose_1.model('Competition', CompetitionSchema);
exports.default = Competition;
