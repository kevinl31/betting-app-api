"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const TeamSchema = new mongoose_1.Schema({
    footballApiId: {
        type: Number,
        unique: true
    },
    name: String,
    shortName: String,
    tla: String,
    website: String,
    founded: Number,
    clubColors: String,
    venue: String,
    lastUpdates: Date,
    currentSeason: {
        seasonId: Number,
        startDate: Date,
        endDate: Date,
        currentMatchday: Number,
    },
    currentCompetition: {
        competitionId: Number,
        name: String
    }
});
const Team = mongoose_1.model('Team', TeamSchema);
exports.default = Team;
