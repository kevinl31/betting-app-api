"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const MatchSchema = new mongoose_1.Schema({
    footballApiId: {
        type: Number,
        unique: true
    },
    date: Date,
    status: {
        type: String,
        index: true
    },
    matchday: {
        type: Number,
        index: true
    },
    score: {
        winner: String,
        fullTime: {
            homeTeam: Number,
            awayTeam: Number
        },
        halfTime: {
            homeTeam: Number,
            awayTeam: Number
        },
    },
    homeTeam: {
        teamId: Number,
        name: String
    },
    awayTeam: {
        teamId: Number,
        name: String
    },
    season: {
        seasonId: Number,
        startDate: Date,
        endDate: Date,
        currentMatchday: Number,
    },
    competition: {
        competitionId: Number,
        name: String
    },
    lastUpdated: Date
});
const Match = mongoose_1.model('Match', MatchSchema);
exports.default = Match;
