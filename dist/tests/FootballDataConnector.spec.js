"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require("mocha");
require('dotenv').config();
const expect = require('expect');
const FootballDataConnector_1 = require("../footballapi/FootballDataConnector");
const footballDataConnector = new FootballDataConnector_1.default();
describe("FootballDataConnector", () => {
    it('Should get the competition 2015', (done) => {
        footballDataConnector.getCompetition('2015').then((res) => {
            expect(res.id).toBe(2015);
        }).then(done);
    });
    it('Should return an error because competition 2081 is not available.', (done) => {
        footballDataConnector.getCompetition('2081').catch((err) => {
            expect(err).toBeInstanceOf(Error);
            expect(err.message).toBe('competition not available');
            done();
        });
    });
    it('Should get 12 competitions, all from AVAILABLE_COMPETITION', (done) => {
        const availableCompetition = process.env.AVAILABLE_COMPETITION.split(',');
        footballDataConnector.getAllCompetition().then((res) => {
            expect(res.length).toBe(8);
            expect(availableCompetition).toContain(String(res[0].id));
        }).then(done);
    });
    it('Should get season 2017-2018 for competion 2015 (Ligue 1)', (done) => {
        const season2017 = {
            id: 17,
            startDate: '2017-08-04',
            endDate: '2018-05-27',
            currentMatchday: 38,
            winner: {
                id: 524,
                name: 'Paris Saint-Germain FC',
                shortName: 'PSG',
                tla: 'PSG',
                crestUrl: 'https://upload.wikimedia.org/wikipedia/fr/thumb/8/86/Paris_Saint-Germain_Logo.svg/130px-Paris_Saint-Germain_Logo.svg'
            }
        };
        footballDataConnector.getCompetitionSeasons('2015').then((res) => {
            expect(res).toContainEqual(season2017);
        }).then(done);
    });
    it('Should get all the teams for competition 2015', (done) => {
        footballDataConnector.getAllSeasonTeams('2015').then((res) => {
            expect(res.length).toBe(20);
        }).then(done);
    });
    it('Should return an error because competition 2081 is not available while fetching teams.', (done) => {
        footballDataConnector.getAllSeasonTeams('2081').catch((err) => {
            expect(err).toBeInstanceOf(Error);
            expect(err.message).toBe('competition not available');
            done();
        });
    });
    it('Should get all the matches for competition 2015', (done) => {
        footballDataConnector.getAllSeasonMatches('2015').then((res) => {
            expect(res.length).toBe(380);
        }).then(done);
    });
    it('Should return an error because competition 2081 is not available while fetching matches.', (done) => {
        footballDataConnector.getAllSeasonMatches('2081').catch((err) => {
            expect(err).toBeInstanceOf(Error);
            expect(err.message).toBe('competition not available');
            done();
        });
    });
    it('Should get all the finished matches for competition 2015', (done) => {
        footballDataConnector.getAllSeasonFinishedMatches('2015').then((res) => {
            res.forEach((item) => expect(item.status).toBe('FINISHED'));
        }).then(done);
    });
    it('Should return an error because competition 2081 is not available while fetching matches.', (done) => {
        footballDataConnector.getAllSeasonMatches('2081').catch((err) => {
            expect(err).toBeInstanceOf(Error);
            expect(err.message).toBe('competition not available');
            done();
        });
    });
    it('Should get the standings for competition 2015 with 20 teams', (done) => {
        footballDataConnector.getCompetitionStandings('2015').then((res) => {
            expect(res.standings.length).toBe(3);
            expect(res.standings[0].table.length).toBe(20);
        }).then(done);
    });
    it('Should return an error because competition 2081 is not available while fetching standings.', (done) => {
        footballDataConnector.getCompetitionStandings('2081').catch((err) => {
            expect(err).toBeInstanceOf(Error);
            expect(err.message).toBe('competition not available');
            done();
        });
    });
    it('Should get the standings for competition 2015 with 10 teams', (done) => {
        footballDataConnector.getMatchdayMatches('2021', '23').then((res) => {
            expect(res.length).toBe(10);
        }).then(done);
    });
    it('Should return an error because competition 2081 is not available while updating matches.', (done) => {
        footballDataConnector.getMatchdayMatches('2081', '23').catch((err) => {
            expect(err).toBeInstanceOf(Error);
            expect(err.message).toBe('competition not available');
            done();
        });
    });
    it('Should return an error because competition 2081 is not available while updating matches.', (done) => {
        footballDataConnector.getMatchdayMatches('2021', null).catch((err) => {
            expect(err).toBeInstanceOf(Error);
            expect(err.message).toBe('matchDay parameters cant be null.');
            done();
        });
    });
});
