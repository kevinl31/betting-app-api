"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
require('dotenv').config();
require("mocha");
const sleep = require('sleep');
const expect = require('expect');
// const { mongoose } = require('../models/mongoose'); 
const FootballDataUpdator_1 = require("../footballapi/FootballDataUpdator");
const footballDataUpdator = new FootballDataUpdator_1.default();
describe("FootballDataUpdator", () => {
    before(function () {
        this.timeout(0);
        sleep.sleep(60);
    });
    it('Should update the matches for matchday and matchday + 1 for all competitions', () => __awaiter(this, void 0, void 0, function* () {
        const updatedMatches = yield footballDataUpdator.updateMatchesData();
        expect(updatedMatches.length).toBe(7);
    })).timeout(250000);
});
