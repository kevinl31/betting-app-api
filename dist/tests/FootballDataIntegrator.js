"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
require('dotenv').config();
require("mocha");
const sleep = require('sleep');
const expect = require('expect');
const { mongoose } = require('../models/mongoose');
const competition_1 = require("../models/schemas/competition");
const team_1 = require("../models/schemas/team");
const match_1 = require("../models/schemas/match");
const season_1 = require("../models/schemas/season");
const standings_1 = require("../models/schemas/standings");
const FootballDataIntegrator_1 = require("../footballapi/FootballDataIntegrator");
const footballDataIntegrator = new FootballDataIntegrator_1.default();
describe("FootballDataIntegrator", () => {
    before(function (done) {
        this.timeout(0);
        sleep.sleep(60);
        const competitionDeletePromise = competition_1.default.deleteMany({})
            .then(() => { return; })
            .catch((err) => console.log(err));
        const teamDeletePromise = team_1.default.deleteMany({})
            .then(() => { return; })
            .catch((err) => console.log(err));
        const matchDeletePromise = match_1.default.deleteMany({})
            .then(() => { return; })
            .catch((err) => console.log(err));
        const seasonDeletePromise = season_1.default.deleteMany({})
            .then(() => { return; })
            .catch((err) => console.log(err));
        const standingsDeletePromise = standings_1.default.deleteMany({})
            .then(() => { return; })
            .catch((err) => console.log(err));
        Promise.all([
            competitionDeletePromise,
            teamDeletePromise,
            matchDeletePromise,
            seasonDeletePromise,
            standingsDeletePromise
        ]).then(() => {
            done();
        });
    });
    it('Should integrate all the competitions in a test database', () => __awaiter(this, void 0, void 0, function* () {
        const compets = yield footballDataIntegrator.integrateCompetitionsData();
        expect(compets.length).toBe(8);
    }));
    it('Should integrate all the teams in a test database', () => __awaiter(this, void 0, void 0, function* () {
        const teams = yield footballDataIntegrator.integrateAllTeamsData(true);
        expect(teams.length).toBe(8);
    })).timeout(120000);
    it('Should integrate all the matches in a test database', () => __awaiter(this, void 0, void 0, function* () {
        const matches = yield footballDataIntegrator.integrateAllMatchesData(true);
        expect(matches.length).toBe(8);
    })).timeout(120000);
    it('Should integrate all the seasons in a test database', () => __awaiter(this, void 0, void 0, function* () {
        const seasons = yield footballDataIntegrator.integrateAllSeasonsData();
        expect(seasons.length).toBe(8);
    })).timeout(120000);
    it('Should integrate all the competition standings in a test database', () => __awaiter(this, void 0, void 0, function* () {
        const standings = yield footballDataIntegrator.integrateAllStandingsData();
        expect(standings.length).toBe(8);
    })).timeout(120000);
});
