"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require('dotenv').config();
const FootballDataUpdator_1 = require("../../footballapi/FootballDataUpdator");
const footballApiUpdator = new FootballDataUpdator_1.default();
const updateStandingPromise = footballApiUpdator.updateStandingsData();
const updateMatchesPromise = footballApiUpdator.updateMatchesData();
Promise.all([
    updateMatchesPromise,
    updateStandingPromise
]).then(() => process.exit(0)).catch((err) => {
    console.log(err);
    process.exit(-1);
});
