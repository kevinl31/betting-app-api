"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require('dotenv').config();
const FootballDataIntegrator_1 = require("../../footballapi/FootballDataIntegrator");
const footballDataIntegrator = new FootballDataIntegrator_1.default();
footballDataIntegrator.integrateCompetitionsData()
    .then(() => {
    const teamIntegrationPromise = footballDataIntegrator.integrateAllTeamsData(true)
        .then(() => { return; })
        .catch((err) => console.log(err));
    const matchIntegrationPromise = footballDataIntegrator.integrateAllMatchesData(true)
        .then(() => { return; })
        .catch((err) => console.log(err));
    const seasonIntegrationPromise = footballDataIntegrator.integrateAllSeasonsData()
        .then(() => { return; })
        .catch((err) => console.log(err));
    const standingsIntegrationPromise = footballDataIntegrator.integrateAllStandingsData()
        .then(() => { return; })
        .catch((err) => console.log(err));
    Promise.all([
        teamIntegrationPromise,
        matchIntegrationPromise,
        seasonIntegrationPromise,
        standingsIntegrationPromise,
    ])
        .then(() => process.exit(0))
        .catch((err) => {
        console.log(err);
        process.exit(-1);
    });
})
    .catch((err) => {
    console.log(err);
    process.exit(-1);
});
