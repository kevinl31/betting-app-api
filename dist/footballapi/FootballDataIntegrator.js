"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const { mongoose } = require('../models/mongoose');
const sleep = require('sleep');
const competition_1 = require("../models/schemas/competition");
const team_1 = require("../models/schemas/team");
const match_1 = require("../models/schemas/match");
const season_1 = require("../models/schemas/season");
const standings_1 = require("../models/schemas/standings");
const FootballDataConnector_1 = require("./FootballDataConnector");
class FootballDataIntegrator {
    constructor() {
        this.footballDataConnector = new FootballDataConnector_1.default();
    }
    integrateCompetitionsData() {
        return __awaiter(this, void 0, void 0, function* () {
            const competitions = yield this.footballDataConnector.getAllCompetition();
            const compets = [];
            competitions.forEach((item) => {
                const competition = new competition_1.default(item);
                competition.footballApiId = item.id;
                competition.currentSeason.seasonId = item.currentSeason.id;
                compets.push(competition);
            });
            return competition_1.default.insertMany(compets);
        });
    }
    integrateCompetitionTeamsData(competition, twoLastSeaon) {
        return __awaiter(this, void 0, void 0, function* () {
            let rawTeams = yield this.footballDataConnector.getAllSeasonTeams(String(competition.footballApiId));
            if (twoLastSeaon) {
                const currentSeasonTeamIds = rawTeams.map((item) => item.id);
                const lastYear = competition.currentSeason.startDate.getFullYear() - 1;
                const lastYearTeams = (yield this.footballDataConnector.getAllSeasonTeams(String(competition.footballApiId), String(lastYear))).filter((item) => currentSeasonTeamIds.indexOf(item.id) === -1);
                rawTeams = rawTeams.concat(lastYearTeams);
            }
            const teamsToInsert = [];
            rawTeams.forEach((item) => {
                const team = new team_1.default(item);
                team.footballApiId = item.id;
                team.currentCompetition = {
                    competitionId: competition.footballApiId,
                    name: competition.name
                };
                team.currentSeason = {
                    seasonId: competition.currentSeason.seasonId,
                    endDate: competition.currentSeason.endDate,
                    startDate: competition.currentSeason.startDate,
                    currentMatchday: competition.currentSeason.currentMatchday
                };
                teamsToInsert.push(team);
            });
            return team_1.default.insertMany(teamsToInsert).then((res) => {
                return res;
            }).catch((err) => { console.log(err); return err; });
        });
    }
    integrateAllTeamsData(twoLastSeaon = false) {
        return __awaiter(this, void 0, void 0, function* () {
            const teamsInsertionPromise = [];
            const competitions = yield competition_1.default.find();
            let i;
            for (i = 0; i < competitions.length; i++) {
                const teamsPromise = yield this.integrateCompetitionTeamsData(competitions[i], twoLastSeaon);
                teamsInsertionPromise.push(teamsPromise);
                sleep.sleep(13);
            }
            return teamsInsertionPromise;
        });
    }
    integrateCompetitionMatchesData(competition, twoLastSeaon) {
        return __awaiter(this, void 0, void 0, function* () {
            const matchesToInsert = [];
            let rawMatches = yield this.footballDataConnector.getAllSeasonMatches(String(competition.footballApiId));
            if (twoLastSeaon) {
                const lastYear = competition.currentSeason.startDate.getFullYear() - 1;
                const lastYearMatches = (yield this.footballDataConnector.getAllSeasonMatches(String(competition.footballApiId), String(lastYear)));
                rawMatches = rawMatches.concat(lastYearMatches);
            }
            rawMatches.forEach((item) => {
                const match = new match_1.default(item);
                match.footballApiId = item.id;
                match.competition = {
                    competitionId: competition.footballApiId,
                    name: competition.name
                };
                match.date = item.utcDate;
                match.season.seasonId = item.season.id;
                match.homeTeam.teamId = item.homeTeam.id;
                match.awayTeam.teamId = item.awayTeam.id;
                matchesToInsert.push(match);
            });
            return match_1.default.insertMany(matchesToInsert).then((res) => {
                return res;
            }).catch((err) => { console.log(err); return err; });
        });
    }
    integrateAllMatchesData(twoLastSeaon = false) {
        return __awaiter(this, void 0, void 0, function* () {
            const matchesInsertionPromise = [];
            const competitions = yield competition_1.default.find();
            let i;
            for (i = 0; i < competitions.length; i++) {
                const matchesPromise = yield this.integrateCompetitionMatchesData(competitions[i], twoLastSeaon);
                matchesInsertionPromise.push(matchesPromise);
                sleep.sleep(13);
            }
            return matchesInsertionPromise;
        });
    }
    integrateCompetitionSeasonsData(competition) {
        return __awaiter(this, void 0, void 0, function* () {
            const rawCompetitions = yield this.footballDataConnector.getCompetition(String(competition.footballApiId));
            const seasonsToInsert = [];
            rawCompetitions.seasons.forEach((item) => {
                const season = new season_1.default(item);
                season.footballApiId = item.id;
                if (item.winner !== null) {
                    season.winner.teamId = item.winner.id;
                }
                season.competition.competitionId = rawCompetitions.id;
                season.competition.name = rawCompetitions.name;
                seasonsToInsert.push(season);
            });
            return season_1.default.insertMany(seasonsToInsert).then((res) => {
                return res;
            }).catch((err) => { console.log(err); return err; });
        });
    }
    integrateAllSeasonsData() {
        return __awaiter(this, void 0, void 0, function* () {
            const seasonsInsertionPromise = [];
            const competitions = yield competition_1.default.find();
            let i;
            for (i = 0; i < competitions.length; i++) {
                const seasonsPromise = yield this.integrateCompetitionSeasonsData(competitions[i]);
                seasonsInsertionPromise.push(seasonsPromise);
                sleep.sleep(13);
            }
            return seasonsInsertionPromise;
        });
    }
    integrateCompetitionStandingsData(competition) {
        return __awaiter(this, void 0, void 0, function* () {
            const rawStandings = yield this.footballDataConnector.getCompetitionStandings(String(competition.footballApiId));
            const standings = new standings_1.default(rawStandings);
            standings.competition.competitionId = rawStandings.competition.id;
            standings.season.seasonId = rawStandings.season.id;
            standings.lastUpdate = new Date();
            return standings.save().then((res) => {
                return res;
            }).catch((err) => { console.log(err); return err; });
        });
    }
    integrateAllStandingsData() {
        return __awaiter(this, void 0, void 0, function* () {
            const standingsInsertionPromise = [];
            const competitions = yield competition_1.default.find();
            let i;
            for (i = 0; i < competitions.length; i++) {
                const standingsPromise = yield this.integrateCompetitionStandingsData(competitions[i]);
                standingsInsertionPromise.push(standingsPromise);
                sleep.sleep(13);
            }
            return standingsInsertionPromise;
        });
    }
}
exports.default = FootballDataIntegrator;
