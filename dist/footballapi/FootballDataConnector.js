"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = require("axios");
const FootballApiConnectionError_1 = require("./../exception/FootballApiConnectionError");
class FootballDataConnector {
    constructor() {
        this.axiosInstance = axios_1.default.create({
            baseURL: "http://api.football-data.org/v2",
            headers: {
                'X-Auth-Token': process.env.API_TOKEN
            }
        });
        this.availableCompetition = process.env.AVAILABLE_COMPETITION.split(',');
    }
    executeRequest(url, params = {}) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.axiosInstance.get(url, params)
                .then((res) => { return res.data; })
                .catch((err) => {
                if (err.status === 404) {
                    return [];
                }
                else {
                    throw new FootballApiConnectionError_1.default('', err);
                }
            });
        });
    }
    getCompetition(competitionId) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.availableCompetition.indexOf(competitionId) === -1) {
                throw new FootballApiConnectionError_1.default('competition not available');
            }
            else {
                return this.executeRequest(`/competitions/${competitionId}`);
            }
        });
    }
    getAllCompetition() {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield this.executeRequest(`/competitions/`);
            const competitions = data.competitions.filter((item) => {
                return this.availableCompetition.indexOf(String(item.id)) !== -1;
            });
            return competitions;
        });
    }
    getCompetitionSeasons(competitionId) {
        return __awaiter(this, void 0, void 0, function* () {
            return (yield this.getCompetition(competitionId)).seasons;
        });
    }
    getAllSeasonTeams(competitionId, year = undefined) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.availableCompetition.indexOf(competitionId) === -1) {
                throw new FootballApiConnectionError_1.default('competition not available');
            }
            else {
                if (year !== undefined) {
                    return (yield this.executeRequest(`/competitions/${competitionId}/teams?season=${year}`)).teams;
                }
                return (yield this.executeRequest(`/competitions/${competitionId}/teams`)).teams;
            }
        });
    }
    getAllSeasonMatches(competitionId, year = undefined) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.availableCompetition.indexOf(competitionId) === -1) {
                throw new FootballApiConnectionError_1.default('competition not available');
            }
            else {
                if (year !== undefined) {
                    return (yield this.executeRequest(`/competitions/${competitionId}/matches?season=${year}`)).matches;
                }
                return (yield this.executeRequest(`/competitions/${competitionId}/matches`)).matches;
            }
        });
    }
    getAllSeasonFinishedMatches(competitionId) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.availableCompetition.indexOf(competitionId) === -1) {
                throw new FootballApiConnectionError_1.default('competition not available');
            }
            else {
                return (yield this.executeRequest(`/competitions/${competitionId}/matches?status=FINISHED`)).matches;
            }
        });
    }
    getMatchdayMatches(competitionId, matchDay) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.availableCompetition.indexOf(competitionId) === -1) {
                throw new FootballApiConnectionError_1.default('competition not available');
            }
            else if (matchDay === null) {
                throw new FootballApiConnectionError_1.default('matchDay parameters cant be null.');
            }
            else {
                return (yield this.executeRequest(`/competitions/${competitionId}/matches?matchday=${matchDay}`)).matches;
            }
        });
    }
    getCompetitionStandings(competitionId) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.availableCompetition.indexOf(competitionId) === -1) {
                throw new FootballApiConnectionError_1.default('competition not available');
            }
            else {
                return (yield this.executeRequest(`/competitions/${competitionId}/standings`));
            }
        });
    }
}
// module.exports = { FootballDataConnector }
exports.default = FootballDataConnector;
