"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const { mongoose } = require('../models/mongoose');
const sleep = require('sleep');
const competition_1 = require("../models/schemas/competition");
const match_1 = require("../models/schemas/match");
const standings_1 = require("../models/schemas/standings");
const FootballDataConnector_1 = require("./FootballDataConnector");
const FootballDataIntegrator_1 = require("./FootballDataIntegrator");
class FootballDataUpdator {
    constructor() {
        this.footballDataConnector = new FootballDataConnector_1.default();
        this.footballDataIntegrator = new FootballDataIntegrator_1.default();
    }
    updateStandingsData() {
        return standings_1.default.deleteMany({})
            .then(() => {
            return this.footballDataIntegrator.integrateAllStandingsData();
        })
            .catch((err) => {
            console.log(err);
        });
    }
    updateMatchesData() {
        return __awaiter(this, void 0, void 0, function* () {
            const competitions = yield competition_1.default.find({});
            const insertionsPromises = [];
            let i;
            for (i = 0; i < competitions.length; i++) {
                const compet = competitions[i];
                if (compet.currentSeason.endDate > new Date()) {
                    const currentMatchday = yield this.footballDataConnector.getMatchdayMatches(String(compet.footballApiId), String(compet.currentSeason.currentMatchday));
                    const nextMatchday = yield this.footballDataConnector.getMatchdayMatches(String(compet.footballApiId), String((compet.currentSeason.currentMatchday.valueOf() + 1)));
                    yield match_1.default.deleteMany({
                        "season.seasonId": compet.currentSeason.seasonId,
                        "matchday": compet.currentSeason.currentMatchday
                    });
                    yield match_1.default.deleteMany({
                        "season.seasonId": compet.currentSeason.seasonId,
                        "matchday": compet.currentSeason.currentMatchday.valueOf() + 1
                    });
                    const matchItems = currentMatchday.concat(nextMatchday).map((item) => {
                        const match = new match_1.default(item);
                        match.footballApiId = item.id;
                        match.competition = {
                            competitionId: compet.footballApiId,
                            name: compet.name
                        };
                        match.date = item.utcDate;
                        match.season.seasonId = item.season.id;
                        match.homeTeam.teamId = item.homeTeam.id;
                        match.awayTeam.teamId = item.awayTeam.id;
                        return match;
                    });
                    insertionsPromises.push(yield match_1.default.insertMany(matchItems));
                    sleep.sleep(13);
                }
            }
            return Promise.all(insertionsPromises);
        });
    }
}
exports.default = FootballDataUpdator;
