"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class FootballApiConnectionError extends Error {
    constructor(msg = '', res = null) {
        const errorMsg = res !== null ? `An error occured during a connection with https://www.football-data.org :\n
        ${res.statusText}: ${res.status}\n
        ${JSON.stringify(res.data)}
        ` : msg;
        super(errorMsg);
    }
}
exports.default = FootballApiConnectionError;
