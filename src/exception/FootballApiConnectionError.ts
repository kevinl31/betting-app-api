import { AxiosResponse } from "axios";


class FootballApiConnectionError extends Error {

    public response: AxiosResponse

    constructor(msg: string = '', res: AxiosResponse = null) {
        
        const errorMsg = res !== null ? `An error occured during a connection with https://www.football-data.org :\n
        ${res.statusText}: ${res.status}\n
        ${JSON.stringify(res.data)}
        ` : msg;
        super(errorMsg);
    }
}

export default FootballApiConnectionError