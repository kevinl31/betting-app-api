import { Document, Schema, Model, model } from 'mongoose';
import { ICompetition } from '../interfaces/competition';

export interface ICompetitionSchema extends ICompetition, Document {}

const CompetitionSchema: Schema = new Schema({
    footballApiId: {
        type: Number,
        unique: true
    },
    name: String,
    emblemUrl: String,
    currentSeason: {
        seasonId: Number,
        startDate: Date,
        endDate: Date,
        currentMatchday: Number
    },
    numberOfAvailableSeason: Number,
    lasUpdate: Date
});

const Competition: Model<ICompetitionSchema> = model<ICompetitionSchema>('Competition', CompetitionSchema);

export default Competition;
