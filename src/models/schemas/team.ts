import { Document, Schema, Model, model } from 'mongoose';
import { ITeam } from '../interfaces/team';

export interface ITeamSchema extends ITeam, Document {}

const TeamSchema: Schema = new Schema({
    footballApiId: {
        type: Number,
        unique: true
    },
    name: String,
    shortName: String,
    tla: String,
    website: String,
    founded: Number,
    clubColors: String,
    venue: String,
    lastUpdates: Date,
    currentSeason: {
        seasonId: Number,
        startDate: Date,
        endDate: Date,
        currentMatchday: Number,
    },
    currentCompetition: {
        competitionId: Number,
        name: String
    }
});

const Team: Model<ITeamSchema> = model<ITeamSchema>('Team', TeamSchema);

export default Team;