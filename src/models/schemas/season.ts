import { Document, Schema, Model, model } from 'mongoose';
import { ISeason } from '../interfaces/season';

export interface ISeasonSchema extends ISeason, Document {}

const SeasonSchema: Schema = new Schema({
    footballApiId: {
        type: Number,
        unique: true
    } ,
    startDate: Date,
    endDate: Date,
    currentMatchDay: Number,
    winner: {
        teamId: Number,
        name: String,
        shortName: String,
        tla: String,
        crestUrl: String
    },
    competition: {
        competitionId: Number,
        name: String
    },
    lastUpdate: Date
});

const Season: Model<ISeasonSchema> = model<ISeasonSchema>('Season', SeasonSchema);

export default Season;