import { Document, Schema, Model, model } from 'mongoose';
import { IMatch } from '../interfaces/match';

export interface IMatchSchema extends IMatch, Document {}

const MatchSchema: Schema = new Schema({
    footballApiId: {
        type: Number,
        unique: true
    },
    date: Date,
    status: {
        type: String,
        index: true
    },
    matchday: {
        type: Number,
        index: true
    },
    score: {
        winner: String,
        fullTime: {
            homeTeam: Number,
            awayTeam: Number
        },
        halfTime: {
            homeTeam: Number,
            awayTeam: Number
        },
    },
    homeTeam: {
        teamId: Number,
        name: String
    },
    awayTeam: {
        teamId: Number,
        name: String
    },
    season: {
        seasonId: Number,
        startDate: Date,
        endDate: Date,
        currentMatchday: Number,
    },
    competition: {
        competitionId: Number,
        name: String
    },
    lastUpdated: Date
});

const Match: Model<IMatchSchema> = model<IMatchSchema>('Match', MatchSchema);

 export default Match;