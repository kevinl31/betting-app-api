import { Document, Schema, Model, model } from 'mongoose';
import { IGlobalStandings, IStandings, ITeamStandingsStats } from '../interfaces/standings';

export interface IGlobalStandingsSchema extends IGlobalStandings, Document {}
export interface IStandingsSchema extends IStandings, Document {}
export interface ITeamStandingsStatsSchema extends ITeamStandingsStats, Document {}

const TeamStandingsStatsSchema: Schema = new Schema({
    position: Number,
    team: {
        id: Number,
        name: String,
        crestUrl: String
    },
    playedGames: Number,
    won: Number,
    draw: Number,
    lost: Number,
    points: Number,
    goalsFor: Number,
    goalsAgainst: Number,
    goalDifference: Number
});

const StandingsSchema: Schema = new Schema({
    stage: String,
    type: String,
    table: [TeamStandingsStatsSchema]
});

const GlobalStandingsSchema: Schema = new Schema({
    competition: {
        competitionId: Number,
        name: String
    },
    season: {
        seasonId: Number,
        startDate: Date,
        endDate: Date,
        currentMatchday: Number
    },
    standings: [StandingsSchema],
    lastUpdate: Date
});

const GlobalStandings: Model<IGlobalStandingsSchema> = 
    model<IGlobalStandingsSchema>('GlobalStandings', GlobalStandingsSchema);

export default GlobalStandings;
