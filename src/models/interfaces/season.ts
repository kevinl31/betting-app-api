
export interface ISeason {
    footballApiId: Number,
    startDate: Date,
    endDate: Date,
    currentMatchDay: Number,
    winner: {
        teamId: Number,
        name: String,
        shortName: String,
        tla: String,
        crestUrl: String
    } | null,
    competition: {
        competitionId: Number,
        name: String
    },
    lastUpdate: Date
}