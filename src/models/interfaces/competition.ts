
export interface ICompetition {
    footballApiId: Number,
    name: String,
    emblemUrl: String,
    currentSeason: {
        seasonId: Number,
        startDate: Date,
        endDate: Date,
        currentMatchday: Number
    } | null,
    numberOfAvailableSeason: Number,
    lasUpdate: Date
}