export interface IMatch {
    footballApiId: Number,
    date: Date,
    status: String,
    matchday: Number,
    score: {
        winner: String,
        fullTime: {
            homeTeam: Number,
            awayTeam: Number
        },
        halfTime: {
            homeTeam: Number,
            awayTeam: Number
        },
    },
    homeTeam: {
        teamId: Number,
        name: String,
    },
    awayTeam: {
        teamId: Number,
        name: String,
    },
    season: {
        seasonId: Number,
        startDate: Date,
        endDate: Date,
        currentMatchday: Number,
    },
    competition: {
        competitionId: Number,
        name: String
    },
    lastUpdated: Date
}