
export interface ITeam {
    footballApiId: Number,
    name: String,
    shortName: String,
    tla: String,
    website: String,
    founded: Number,
    clubColors: String,
    venue: String,
    lastUpdates: Date,
    currentSeason: {
        seasonId: Number,
        startDate: Date,
        endDate: Date,
        currentMatchday: Number,
    },
    currentCompetition: {
        competitionId: Number,
        name: String
    }
}