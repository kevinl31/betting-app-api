export interface IGlobalStandings {
    competition: {
        competitionId: Number,
        name: String
    },
    season: {
        seasonId: Number,
        startDate: Date,
        endDate: Date,
        currentMatchday: Number
    },
    standings: IStandings[],
    lastUpdate: Date
}

export interface IStandings {
    stage: String,
    type: String,
    table: ITeamStandingsStats[]
}


export interface ITeamStandingsStats {
    position: Number,
    team: {
        id: Number,
        name: String,
        crestUrl: String
    },
    playedGames: Number,
    won: Number,
    draw: Number,
    lost: Number,
    points: Number,
    goalsFor: Number,
    goalsAgainst: Number,
    goalDifference: Number,
}