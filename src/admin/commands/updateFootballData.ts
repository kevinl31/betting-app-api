require('dotenv').config();

import FootballDataUpdator from '../../footballapi/FootballDataUpdator';

const footballApiUpdator = new FootballDataUpdator();

const updateStandingPromise = footballApiUpdator.updateStandingsData();
const updateMatchesPromise = footballApiUpdator.updateMatchesData();

Promise.all([
    updateMatchesPromise,
    updateStandingPromise
]).then(() => process.exit(0)).catch((err) => {
    process.exit(-1);
});
