require('dotenv').config()

import 'mocha';

const sleep = require('sleep');
const expect = require('expect');
// const { mongoose } = require('../models/mongoose'); 

import FootballDataUpdator from '../footballapi/FootballDataUpdator';

const footballDataUpdator = new FootballDataUpdator();

describe("FootballDataUpdator", () => {

    before(function () {
        this.timeout(0);
        sleep.sleep(60);
    });

    it('Should update the matches for matchday and matchday + 1 for all competitions', async () =>  {        
        const updatedMatches = await footballDataUpdator.updateMatchesData();
        expect(updatedMatches.length).toBe(7);
    }).timeout(250000);
})
