require('dotenv').config()

import 'mocha';

const sleep = require('sleep');
const expect = require('expect');
const { mongoose } = require('../models/mongoose'); 

import Competition from '../models/schemas/competition';
import Team from '../models/schemas/team';
import Match from '../models/schemas/match';
import Season from '../models/schemas/season';
import GlobalStandings from '../models/schemas/standings';
import FootballDataIntegrator from '../footballapi/FootballDataIntegrator';

const footballDataIntegrator = new FootballDataIntegrator();

describe("FootballDataIntegrator", () => {

    before(function (done) {
        this.timeout(0);
        sleep.sleep(60);
        const competitionDeletePromise = Competition.deleteMany({})
            .then(() => {return})
            .catch((err) => console.log(err));
        const teamDeletePromise = Team.deleteMany({})
            .then(() => {return})
            .catch((err) => console.log(err));
        const matchDeletePromise = Match.deleteMany({})
            .then(() => {return})
            .catch((err) => console.log(err));
        const seasonDeletePromise = Season.deleteMany({})
            .then(() => {return})
            .catch((err) => console.log(err));
        const standingsDeletePromise = GlobalStandings.deleteMany({})
            .then(() => {return})
            .catch((err) => console.log(err));
        Promise.all([
            competitionDeletePromise,
            teamDeletePromise,
            matchDeletePromise,
            seasonDeletePromise,
            standingsDeletePromise
        ]).then(() => {
            done()
        });
    });

    it('Should integrate all the competitions in a test database', async () =>  {        
        const compets = await footballDataIntegrator.integrateCompetitionsData();
        expect(compets.length).toBe(8);
    });

    it('Should integrate all the teams in a test database', async () =>  {        
        const teams = await footballDataIntegrator.integrateAllTeamsData(true);
        expect(teams.length).toBe(8);
    }).timeout(120000);

    it('Should integrate all the matches in a test database', async () =>  {        
        const matches = await footballDataIntegrator.integrateAllMatchesData(true);
        expect(matches.length).toBe(8);
    }).timeout(120000);

    it('Should integrate all the seasons in a test database', async () =>  {        
        const seasons = await footballDataIntegrator.integrateAllSeasonsData();
        expect(seasons.length).toBe(8);
    }).timeout(120000);

    it('Should integrate all the competition standings in a test database', async () =>  {        
        const standings = await footballDataIntegrator.integrateAllStandingsData();
        expect(standings.length).toBe(8);
    }).timeout(120000);
});

