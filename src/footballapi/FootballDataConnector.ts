import axios, { AxiosInstance, AxiosResponse } from 'axios';

import FootballDataConnectorError from './../exception/FootballApiConnectionError';

class FootballDataConnector {
    public axiosInstance : AxiosInstance;
    public availableCompetition: String[];

    constructor() {
        this.axiosInstance = axios.create({
            baseURL: "http://api.football-data.org/v2",
            headers: {
                'X-Auth-Token': process.env.API_TOKEN
            }
        });
        this.availableCompetition = process.env.AVAILABLE_COMPETITION.split(',');
    }

    private async executeRequest(url: string, params: any = {}): Promise<any> {
        return this.axiosInstance.get(url, params)
            .then((res) => { return res.data })
            .catch((err) => { 
                if(err.status === 404){
                    return []; 
                }
                else {
                    throw new FootballDataConnectorError('', err);
                }
            });
    }

    public async getCompetition(competitionId: String): Promise<any> {
        if (this.availableCompetition.indexOf(competitionId) === -1){
            throw new FootballDataConnectorError('competition not available');
        } else {
            return this.executeRequest(`/competitions/${competitionId}`)
        }
    }

    public async getAllCompetition(): Promise<any[]> {
        
        const data = await this.executeRequest(`/competitions/`);

        const competitions = data.competitions.filter((item) => {
            return this.availableCompetition.indexOf(String(item.id)) !== -1;
        });
        return competitions
    }

    public async getCompetitionSeasons(competitionId: String): Promise<any[]> {
        return (await this.getCompetition(competitionId)).seasons;
    }

    public async getAllSeasonTeams(competitionId: String, year: String = undefined): Promise<any[]> {

        if (this.availableCompetition.indexOf(competitionId) === -1){
            throw new FootballDataConnectorError('competition not available');
        } else {
            if (year !== undefined) {
                return (await this.executeRequest(`/competitions/${competitionId}/teams?season=${year}`)).teams
            }
            return (await this.executeRequest(`/competitions/${competitionId}/teams`)).teams;
        }
    }

    public async getAllSeasonMatches(competitionId: String, year: String = undefined): Promise<any[]> {

        if (this.availableCompetition.indexOf(competitionId) === -1){
            throw new FootballDataConnectorError('competition not available');
        } else {
            if (year !== undefined) {
                return (await this.executeRequest(`/competitions/${competitionId}/matches?season=${year}`)).matches;
            }
            return (await this.executeRequest(`/competitions/${competitionId}/matches`)).matches;
        }
    }

    public async getAllSeasonFinishedMatches(competitionId: String): Promise<any[]> {

        if (this.availableCompetition.indexOf(competitionId) === -1){
            throw new FootballDataConnectorError('competition not available');
        } else {
            return (await this.executeRequest(`/competitions/${competitionId}/matches?status=FINISHED`)).matches;
        }
    }

    public async getMatchdayMatches(competitionId: String, matchDay: String): Promise<any[]> {
        if (this.availableCompetition.indexOf(competitionId) === -1){
            throw new FootballDataConnectorError('competition not available');
        } else if (matchDay === null){
            throw new FootballDataConnectorError('matchDay parameters cant be null.');
        }else {
            return (await this.executeRequest(`/competitions/${competitionId}/matches?matchday=${matchDay}`)).matches;
        }
    }

    public async getCompetitionStandings(competitionId: String): Promise<any> {

        if (this.availableCompetition.indexOf(competitionId) === -1){
            throw new FootballDataConnectorError('competition not available');
        } else {
            return (await this.executeRequest(`/competitions/${competitionId}/standings`));
        }
    }
}

// module.exports = { FootballDataConnector }
export default FootballDataConnector;