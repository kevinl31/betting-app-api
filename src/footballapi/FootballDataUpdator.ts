const { mongoose } = require('../models/mongoose');
const sleep = require('sleep');

import Competition from '../models/schemas/competition';
import Match, { IMatchSchema } from '../models/schemas/match';
import GlobalStandings, {IGlobalStandingsSchema} from '../models/schemas/standings';

import FootballDataConnector from './FootballDataConnector';
import FootballDataIntegrator from './FootballDataIntegrator';


class FootballDataUpdator {
    public footballDataConnector: FootballDataConnector;
    public footballDataIntegrator: FootballDataIntegrator;

    constructor() {
        this.footballDataConnector = new FootballDataConnector();
        this.footballDataIntegrator = new FootballDataIntegrator();
    }

    public updateStandingsData(): Promise<void | IGlobalStandingsSchema[]> {
        return GlobalStandings.deleteMany({})
            .then(() => {
                return this.footballDataIntegrator.integrateAllStandingsData();
            })
            .catch((err) => {
                console.log(err);
            });
    }

    public async updateMatchesData(): Promise<IMatchSchema[][]>{
        const competitions = await Competition.find({});
        const insertionsPromises: IMatchSchema[][] = [];

        let i: number;
        for (i = 0; i < competitions.length; i++) {
            const compet = competitions[i];
            if (compet.currentSeason.endDate > new Date()) {
                const currentMatchday = await this.footballDataConnector.getMatchdayMatches(
                    String(compet.footballApiId), String(compet.currentSeason.currentMatchday)
                );
                const nextMatchday = await this.footballDataConnector.getMatchdayMatches(
                    String(compet.footballApiId), String((compet.currentSeason.currentMatchday.valueOf() + 1))
                );
                await Match.deleteMany({
                    "season.seasonId": compet.currentSeason.seasonId,
                    "matchday": compet.currentSeason.currentMatchday
                })
                await Match.deleteMany({
                    "season.seasonId": compet.currentSeason.seasonId,
                    "matchday": compet.currentSeason.currentMatchday.valueOf() + 1
                })
                const matchItems = currentMatchday.concat(nextMatchday).map((item) => {
                    const match = new Match(item);
                    match.footballApiId = item.id;
                    match.competition = {
                        competitionId: compet.footballApiId,
                        name: compet.name
                    };
                    match.date = item.utcDate;
                    match.season.seasonId = item.season.id;
                    match.homeTeam.teamId = item.homeTeam.id;
                    match.awayTeam.teamId = item.awayTeam.id;
                    return match
                });
                
                insertionsPromises.push(await Match.insertMany(matchItems));
                sleep.sleep(13);
            }
        }

        return Promise.all(insertionsPromises);
    }
}

export default FootballDataUpdator;