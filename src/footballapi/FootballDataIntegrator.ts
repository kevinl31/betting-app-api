const { mongoose } = require('../models/mongoose');
const sleep = require('sleep');

import Competition, { ICompetitionSchema } from '../models/schemas/competition';
import Team, { ITeamSchema } from '../models/schemas/team';
import Match, { IMatchSchema } from '../models/schemas/match';
import Season, { ISeasonSchema } from '../models/schemas/season';
import GlobalStandings, { IGlobalStandingsSchema} from '../models/schemas/standings';

import FootballDataConnector from './FootballDataConnector';

class FootballDataIntegrator {
    public footballDataConnector: FootballDataConnector;

    constructor() {
        this.footballDataConnector = new FootballDataConnector();
    }

    public async integrateCompetitionsData(): Promise<ICompetitionSchema[]> {

        const competitions = await this.footballDataConnector.getAllCompetition()
        const compets = [];

        competitions.forEach((item) => {
            const competition = new Competition(item);
            competition.footballApiId = item.id;
            competition.currentSeason.seasonId = item.currentSeason.id;
            compets.push(competition);
        });

        return Competition.insertMany(compets);
    }

    private async integrateCompetitionTeamsData(
        competition: ICompetitionSchema,
        twoLastSeaon: boolean
    ): Promise<ITeamSchema[]> {

        let rawTeams = await this.footballDataConnector.getAllSeasonTeams(String(competition.footballApiId));
        
        if (twoLastSeaon) {
            const currentSeasonTeamIds = rawTeams.map((item) => item.id);
            const lastYear = competition.currentSeason.startDate.getFullYear() - 1;
            const lastYearTeams = (await this.footballDataConnector.getAllSeasonTeams(
                String(competition.footballApiId), String(lastYear)
            )).filter((item) => currentSeasonTeamIds.indexOf(item.id) === -1);
            
            rawTeams = rawTeams.concat(lastYearTeams);
        }
        
        const teamsToInsert = [];
        rawTeams.forEach((item) => {
            const team = new Team(item);
            team.footballApiId = item.id;
            team.currentCompetition = {
                competitionId: competition.footballApiId,
                name: competition.name
            };
            team.currentSeason = {
                seasonId: competition.currentSeason.seasonId,
                endDate: competition.currentSeason.endDate,
                startDate: competition.currentSeason.startDate,
                currentMatchday: competition.currentSeason.currentMatchday
            };
            teamsToInsert.push(team);
        });

        return Team.insertMany(teamsToInsert).then((res) => {
            return res
        }).catch((err) => { console.log(err); return err });
    }

    public async integrateAllTeamsData(twoLastSeaon: boolean = false): Promise<ITeamSchema[][]> {
        const teamsInsertionPromise: ITeamSchema[][] = [];
        const competitions = await Competition.find();

        let i: number;
        for (i = 0; i < competitions.length; i++) {
            const teamsPromise = await this.integrateCompetitionTeamsData(competitions[i], twoLastSeaon);
            teamsInsertionPromise.push(teamsPromise);
            sleep.sleep(13);
        }
        return teamsInsertionPromise;
    }

    private async integrateCompetitionMatchesData(
        competition: ICompetitionSchema,
        twoLastSeaon: boolean
    ): Promise<IMatchSchema[]> {

        const matchesToInsert = [];
        let rawMatches = await this.footballDataConnector.getAllSeasonMatches(String(competition.footballApiId));
        
        if (twoLastSeaon) {
            const lastYear = competition.currentSeason.startDate.getFullYear() - 1;
            const lastYearMatches = (await this.footballDataConnector.getAllSeasonMatches(
                String(competition.footballApiId), String(lastYear)
            ));
            rawMatches = rawMatches.concat(lastYearMatches);
        }
        
        rawMatches.forEach((item) => {
            const match = new Match(item);
            match.footballApiId = item.id;
            match.competition = {
                competitionId: competition.footballApiId,
                name: competition.name
            };
            match.date = item.utcDate;
            match.season.seasonId = item.season.id;
            match.homeTeam.teamId = item.homeTeam.id;
            match.awayTeam.teamId = item.awayTeam.id;
            matchesToInsert.push(match);
        });

        return Match.insertMany(matchesToInsert).then((res) => {
            return res
        }).catch((err) => { console.log(err); return err });
    }

    public async integrateAllMatchesData(twoLastSeaon: boolean = false): Promise<IMatchSchema[][]> {
        const matchesInsertionPromise: IMatchSchema[][] = [];
        const competitions = await Competition.find();

        let i: number;
        for (i = 0; i < competitions.length; i++) {
            const matchesPromise = await this.integrateCompetitionMatchesData(competitions[i], twoLastSeaon);
            matchesInsertionPromise.push(matchesPromise);
            sleep.sleep(13);
        }

        return matchesInsertionPromise;
    }

    private async integrateCompetitionSeasonsData(competition: ICompetitionSchema): Promise<ISeasonSchema[]> {
        const rawCompetitions = await this.footballDataConnector.getCompetition(String(competition.footballApiId));
        const seasonsToInsert: ISeasonSchema[] = [];

        rawCompetitions.seasons.forEach((item) => {
            const season = new Season(item);
            season.footballApiId = item.id;
            if (item.winner !== null) {
                season.winner.teamId = item.winner.id;
            }
            season.competition.competitionId = rawCompetitions.id;
            season.competition.name = rawCompetitions.name;
            seasonsToInsert.push(season);
        });

        return Season.insertMany(seasonsToInsert).then((res) => {
            return res;
        }).catch((err) => { console.log(err); return err });

    }

    public async integrateAllSeasonsData(): Promise<ISeasonSchema[][]> {
        const seasonsInsertionPromise: ISeasonSchema[][] = [];
        const competitions = await Competition.find();

        let i: number;
        for (i = 0; i < competitions.length; i++) {
            const seasonsPromise = await this.integrateCompetitionSeasonsData(competitions[i]);
            seasonsInsertionPromise.push(seasonsPromise);
            sleep.sleep(13);
        }

        return seasonsInsertionPromise;
    }

    private async integrateCompetitionStandingsData(competition: ICompetitionSchema): Promise<IGlobalStandingsSchema> {
        const rawStandings = await this.footballDataConnector.getCompetitionStandings(String(competition.footballApiId));
        
        const standings = new GlobalStandings(rawStandings);
        standings.competition.competitionId = rawStandings.competition.id;
        standings.season.seasonId = rawStandings.season.id;
        standings.lastUpdate = new Date();

        return standings.save().then((res) => {
            return res;
        }).catch((err) => { console.log(err); return err });

    }

    public async integrateAllStandingsData(): Promise<IGlobalStandingsSchema[]> {
        const standingsInsertionPromise: IGlobalStandingsSchema[] = [];
        const competitions = await Competition.find();

        let i: number;
        for (i = 0; i < competitions.length; i++) {
            const standingsPromise = await this.integrateCompetitionStandingsData(competitions[i]);
            standingsInsertionPromise.push(standingsPromise);
            sleep.sleep(13);
        }

        return standingsInsertionPromise;
    }
}

export default FootballDataIntegrator;
