import { Controller, Get, Param } from '@nestjs/common';
import { StandingsService } from './standings.service';
import {IGlobalStandingsSchema} from '../../models/schemas/standings';

@Controller('standings')
export class StandingsController {

    constructor(private readonly standingsService: StandingsService) {}

    @Get()
    async findAll(): Promise<IGlobalStandingsSchema[]>{
        return this.standingsService.findAll();
    }

    @Get(':id')
    async findOne(@Param('id') id: number): Promise<IGlobalStandingsSchema> {
        return this.standingsService.findStandingsById(id);
    }
}
