const { mongoose } = require('../../models/mongoose');

import { Injectable } from '@nestjs/common';
import GlobalStandings, {IGlobalStandingsSchema} from '../../models/schemas/standings';

@Injectable()
export class StandingsService {

    async findAll(): Promise<IGlobalStandingsSchema[]> {
        return GlobalStandings.find({}).then((docs) => {
            return docs;
        }).catch((err) => {
            return err;
        });
    }

    async findStandingsById( id: number ): Promise<IGlobalStandingsSchema> {
        return GlobalStandings.findOne({'competition.competitionId': id}).then((res) => res);;
    }
}
